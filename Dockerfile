# This is simply a no-op Dockerfile so that this repo can successfully pass CI in gitlab
FROM scratch

# We have to do _something_ so that docker build successfully exits
ADD .gitlab-ci.yml /
